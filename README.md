# terraform-aws-asg

## Prerequisites

- [Terraform](https://releases.hashicorp.com/terraform/). This module currently tested on `0.12.26`

## Quick Start

Terraform module to create ASG with EC2 healthcheck and minimum custom configs

### Autoscaling Group

```hcl
module "abc_dev" {
  source  = "git::https://gitlab.com/rizkidoank/terraform-aws-asg.git"
  version = "v0.1.0"
  
  asg_name                  = "example-dev"
  asg_subnet_ids            = ["subnet-12345"]
  environment               = "test"
  asg_image_id              = "ami-08569b978cc4dfa10"
  asg_key_name              = aws_key_pair.this.key_name
  asg_max_size              = 2
  asg_min_size              = 1
  asg_instance_type         = "t3a.nano"
  asg_instance_profile_name = aws_iam_instance_profile.this.name
  asg_security_groups       = [data.aws_security_group.default.id]
}
```

### Module

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| asg\_default\_cooldown | ASG default cooldown | `number` | `10` | no |
| asg\_health\_check\_grace\_period | Healthcheck grace period for the ASG | `number` | `300` | no |
| asg\_image\_id | Image to be used by ASG instances | `string` | n/a | yes |
| asg\_instance\_profile\_name | Instance profile for ASG instances | `string` | n/a | yes |
| asg\_instance\_type | Instance type for the ASG instances | `string` | n/a | yes |
| asg\_key\_name | Keypair used for the ASG | `string` | n/a | yes |
| asg\_max\_size | ASG max capacity | `number` | n/a | yes |
| asg\_min\_size | ASG min capacity | `number` | n/a | yes |
| asg\_name | Name to be used by ASG instances | `string` | n/a | yes |
| asg\_security\_groups | List of SG to be used by ASG instances | `list` | n/a | yes |
| asg\_subnet\_ids | Subnet ids where ASG will be deployed | `list` | n/a | yes |
| asg\_termination\_policies | ASG applied termination policies | `list` | <pre>[<br>  "Default"<br>]</pre> | no |
| asg\_volume\_size | ASG instances volume size | `number` | `8` | no |
| environment | Environment for the ASG | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| asg\_launch\_template\_id | n/a |
| asg\_launch\_template\_name | n/a |
| asg\_max\_size | n/a |
| asg\_min\_size | n/a |
| asg\_name | n/a |
| asg\_subnet\_ids | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
